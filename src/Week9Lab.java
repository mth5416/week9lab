/**
 * Week9Lab.java
 * Creates a GUI that increments and decrements a number within a label
 *
 * @author Mason Hicks
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Week9Lab extends Application {

    private int count;
    Label label;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.count = 0;

        this.label = new Label(Integer.toString(count));
        this.label.setMinWidth(50);
        label.setAlignment(Pos.CENTER);
        Button decrement = new Button("Decrease");
        Button increment = new Button("Increase");
        Button crazy = new Button("???");

        decrement.setOnAction(new DecrementButtonHandler());
        increment.setOnAction(new IncrementButtonHandler());
        crazy.setOnAction(new RandomButtonHandler());

        HBox hbox = new HBox(10,decrement,label,increment);
        hbox.setAlignment(Pos.CENTER);
        VBox vbox = new VBox(10,hbox,crazy);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(10));
        vbox.setMinWidth(300);

        primaryStage.setTitle("Week 9 Lab");
        primaryStage.show();

        Scene scene = new Scene(vbox);
        primaryStage.setScene(scene);
    }

    public static void main(String[] args){
        launch(args);
    }

    class IncrementButtonHandler implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            count++;
            label.setText(Integer.toString(count));
        }
    }

    class DecrementButtonHandler implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            count--;
            label.setText(Integer.toString(count));
        }
    }

    class RandomButtonHandler implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            count += (int)(Math.random()*100)-50;
            label.setText(Integer.toString(count));
        }
    }
}
